var tagging = require('./code/tagging')

window.addEventListener('DOMContentLoaded', init);

function init() {
  loadChanelFont();

  tagging.init();
}

function loadChanelFont() {
  Typekit.load({
    async: true,
  })
}