! function (e) {
	var t = {};

	function n(r) {
		if (t[r]) return t[r].exports;
		var i = t[r] = {
			i: r,
			l: !1,
			exports: {}
		};
		return e[r].call(i.exports, i, i.exports, n), i.l = !0, i.exports
	}
	n.m = e, n.c = t, n.d = function (e, t, r) {
		n.o(e, t) || Object.defineProperty(e, t, {
			enumerable: !0,
			get: r
		})
	}, n.r = function (e) {
		"undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
			value: "Module"
		}), Object.defineProperty(e, "__esModule", {
			value: !0
		})
	}, n.t = function (e, t) {
		if (1 & t && (e = n(e)), 8 & t) return e;
		if (4 & t && "object" == typeof e && e && e.__esModule) return e;
		var r = Object.create(null);
		if (n.r(r), Object.defineProperty(r, "default", {
			enumerable: !0,
			value: e
		}), 2 & t && "string" != typeof e)
			for (var i in e) n.d(r, i, function (t) {
				return e[t]
			}.bind(null, i));
		return r
	}, n.n = function (e) {
		var t = e && e.__esModule ? function () {
			return e.default
		} : function () {
			return e
		};
		return n.d(t, "a", t), t
	}, n.o = function (e, t) {
		return Object.prototype.hasOwnProperty.call(e, t)
	}, n.p = "", n(n.s = 1)
}([
	function (e, t) {
		e.exports = {
			init: function () {
				dataLayer.push({
					event: "virtualpageview",
					division: "",
					page_axis: "",
					region_chanel: "us",
					locale: "en",
					page_type: "appointment booking",
					page: window.location.href.split("/us")[1],
					divisionFlag: "",
					category: "service"
				})
			}
		}
	},
	function (e, t, n) {
		n(2), e.exports = n(0)
	},
	function (e, t, n) {
		var r = n(0);
		window.addEventListener("DOMContentLoaded", function () {
			(function e() {
				window.jQuery ? (! function () {
					var e = {
							"/fashion/": ["fashion", "jewelry", "watches", "fragrance", "makeup", "skincare"],
							"/fine-jewelry/": ["jewelry", "watches", "fashion", "fragrance", "makeup", "skincare"],
							"/high-jewelry/": ["jewelry", "watches", "fashion", "fragrance", "makeup", "skincare"],
							"/watches/": ["watches", "jewelry", "fashion", "fragrance", "makeup", "skincare"],
							"/fragrance/": ["fragrance", "makeup", "skincare", "fashion", "jewelry", "watches"],
							"/makeup/": ["makeup", "skincare", "fragrance", "fashion", "jewelry", "watches"],
							"/skincare/": ["skincare", "makeup", "fragrance", "fashion", "jewelry", "watches"],
							"/care-services/": ["watches", "jewelry", "fashion", "makeup", "skincare", "fragrance"],
							"/care_services_hub_v2/": ["watches", "jewelry", "fashion", "makeup", "skincare", "fragrance"]
						},
						t = window.sessionStorage.mtLastVisitedURL;
					if (t)
						for (var n = Object.keys(e), r = 0; r < n.length; r++)
							if (t.includes(n[r]))
								for (var i = e[n[r]], a = 0; a < i.length; a++) {
									var o = `rock-center-event-${i[a]}-service`,
										c = $(`#${o}`);
									$(`#${o}`).remove(), $(".ft-rock-center-event-content-carousel").append(c)
								}
				}(), $.when($.getScript("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"), $.Deferred(function (e) {
					$(e.resolve)
				})).done(c)) : setTimeout(function () {
					e()
				}, 50)
			})(), r.init(),
			function () {
				try {
					Typekit.load()
				} catch (e) {
					console.log(e)
				}
			}()
		}), window.dataLayer = window.dataLayer || [], window.ftCurrentSlideIndex = 0;
		const i = ["https://www.chanel.com/us/", "https://origin-prd-one-www.chanel.com/us/", "https://origin-prd-one-staged-www.chanel.com/us/", "https://origin-prd-one-staged-emea-www.chanel.com/us/"],
			a = function () {
				for (var e = "uat", t = 0; t < i.length; t++) window.location.href.includes(i[t]) && (e = "prod");
				return e
			}();

		function o() {
			return "prod" === a
		}

		function c() {
			var e = $(".ft-rock-center-event-content-carousel");
			e.slick({
				dots: !1,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: !1,
				nextArrow: $(this).find(".ft-rock-center-event-content-carousel__next-button"),
				prevArrow: $(this).find(".ft-rock-center-event-content-carousel__previous-button"),
			}), window.ftCurrentSlideIndex = window.ftCurrentSlideIndex + 1, s(), e.on("afterChange", function (e, t, n) {
				window.ftCurrentSlideIndex = n + 1, s(), $(".slick-current button").focus(), d()
			}),
			function () {
				const e = o() ? "48bbe13d-59ae-4ca2-b508-00a88f699575" : "1ce4cf08-2e59-4021-8b5d-a47fb5c8253f",
					t = o() ? "dc525471-dab6-466c-9fdd-1a9e5240b73b" : "aaf7a24d-56aa-4ce0-8bdf-401af07f4049",
					n = o() ? "e9702852-fa1d-431e-9deb-6aedc406bac4" : "1dcc23bd-7c80-4d89-8ac2-3ae2af5ae39c",
					r = o() ? "f93c6f62-cb86-4d70-afdd-d7a1f281d6b3" : "7ee530e9-e728-40c0-bf01-e230e4cb2dff",
					i = o() ? "c5ed433d-f153-476f-90e6-0062c7cd1000" : "b96e3166-7969-4c77-868f-f2f91d79a733",
					c = o() ? "2197d58b-340c-4fd9-b434-35380d1280ab" : "085619e1-2d84-4e7d-a6c0-2989c2ac223c",
					s = o() ? "32a0d447-2847-493e-a16f-d1f869dc8b04" : "5fab8ae0-23d9-4e0f-9c21-0e384604334a",
					l = o() ? "b67f41f1-912f-4f57-b0ca-51f41b76b04a" : "6a422e99-c377-4e4b-b5d7-7b8a5e32010b";

				function f(e) {
					! function (e) {
						var t = e.id,
							n = document.querySelector(t),
							r = function (e) {
								var t = "monetate-brickwork-button-" + Math.floor(2e3 * Math.random());
								return '<button class="ft-button-black" aria-label="Select a service - ' + e.name + '"  id="' + t + '"> <span><span>' + e.name + "</span></span> </button>"
							}(e);
						n.innerHTML = r, console.log("brickwork option: ", e)
					}({
						id: "#brickwork-" + e.id + "-appt-booking",
						filterServiceIds: e.filterServiceIds,
						startsWith: e.startsWith,
						name: e.name
					}), $("#brickwork-" + e.id + "-appt-booking").click(() => {
						u("book an appointment", e.id, "click")
					}),
					function (e) {
						var t = document.querySelector(e.id),
							n = function (e) {
								var t = e.id.substring(1),
									n = a,
									r = e.startsWith || "location",
									i = function (e) {
										var t = [];
										for (var n in e) e.hasOwnProperty(n) && e[n] && t.push(n + "=" + e[n]);
										return t.join("&")
									}({
										domElementId: t,
										startsWith: r,
										locationId: e.initLocation || "",
										serviceId: e.initService || ""
									});
								return function (e) {
									var t = document.createElement("script");
									return t.type = "text/javascript", t.src = e.fullUrl, console.log("getScript option parameter: ", e), console.log("option service ids: ", e.filterServiceIds), t.onload = function () {
										window.brickworkWidgets = window.__brickworkWidgets__ || {}, window.brickworkWidgets[e.idNameOnly] = window.brickworkWidgets[e.idNameOnly] || {}, window.brickworkWidgets[e.idNameOnly].ApptLayout = {
											startsWith: e.startsWith,
											locale: "en",
											hideLocation: !1
										}, window.brickworkWidgets[e.idNameOnly].ApptServicePage = {
											filterServiceIds: e.filterServiceIds,
											showServiceIds: e.showServiceIds
										}
									}, console.log("script before return: ", t), t
								}({
									fullUrl: {
										uat: "https://chanel-uat.brickworksoftware.com/render/widgets?",
										prod: "https://chanel.brickworksoftware.com/render/widgets?"
									}[n] + i,
									idNameOnly: t,
									startsWith: r,
									filterServiceIds: e.filterServiceIds || [],
									showServiceIds: e.showServiceIds || []
								})
							}(e);
						t.append(n)
					}({
						id: "#rock-center-event-" + e.id + "-service",
						filterServiceIds: e.filterServiceIds,
						startsWith: e.startsWith,
						name: e.name
					}), $("#rock-center-event-" + e.id + "-service").click(() => {
						u("book an appointment", e.id, "click")
					})
				}

				function u(e, t, n) {
					dataLayer.push({
						eventCategory: e,
						eventAction: t,
						eventLabel: n,
						event: "uaevent"
					})
				}
				f({
					id: "fashion",
					filterServiceIds: [s, l],
					startsWith: "service",
					name: "fashion"
				}), f({
					id: "skincare",
					filterServiceIds: [i, c],
					startsWith: "service",
					name: "skincare"
				}), f({
					id: "makeup",
					filterServiceIds: [i, c],
					startsWith: "service",
					name: "makeup"
				}), f({
					id: "fragrance",
					filterServiceIds: [i, c],
					startsWith: "service",
					name: "fragrance"
				}), f({
					id: "watches",
					filterServiceIds: [e, r, n, t],
					startsWith: "service",
					name: "watches and fine jewelry"
				}), $(".ft-rock-center-event-content-carousel__previous-button").click(() => {
					u("carousel", "home", "swipe")
				}), $(".ft-rock-center-event-content-carousel__next-button").click(() => {
					u("carousel", "home", "swipe")
				}), d()
			}()
		}

		function s() {
			var e = document.getElementsByClassName("slick-slide").length,
				t = window.ftCurrentSlideIndex;
			window.matchMedia("(min-width: 600px)").matches && (e = Math.ceil(e / 5), t = window.ftCurrentSlideIndex > 5 ? 2 : 1), document.getElementById("rock-center-event-carousel-index").innerHTML = t + '<span class="is-sr-only">of</span><span aria-hidden="true">/</span>' + e
		}

		function d() {
			for (var e = document.getElementsByClassName("slick-slide"), t = 0; t < e.length; t++) {
				var n = e[t];
				n.classList.contains("slick-active") ? n.querySelector("button").removeAttribute("tabIndex") : n.querySelector("button").setAttribute("tabIndex", "-1")
			}
		}
	}
]);