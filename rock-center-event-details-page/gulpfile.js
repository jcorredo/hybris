var fs = require('fs')
var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var file = require('gulp-file')
var path = require('path')
var plumber = require('gulp-plumber');

var isProd = process.env.NODE_ENV === "production"

const config = {
    jsPath: './app/**/*.js',
    htmlPath: './app/**/*.pug',
    sassPath: './app/**/*.scss',
    imgsPath: './app/imgs/**/*.{gif,jpg,png,svg,mp4}',
    i18nPath: './app/data/i18n.json',
    cmsRefPath: './app/data/ref.json'
}

function updateTranslation() {
    var i18n = getI18nFile(config.i18nPath)
    var keys = Object.keys(i18n);

    if(isProd){
        for (let i = 0; i < keys.length; i++) {
            var key = keys[i];
            i18n[key] = '${' + key + '}'
        }
    }

    return i18n;
}


function getReferenceObject() {
    var fileObj = getI18nFile(config.cmsRefPath)
    var keys = Object.keys(fileObj);

    for (let i = 0; i < keys.length; i++) {
        var key = keys[i];

        if(isProd) {
            fileObj[key] = fileObj[key].cmsValue
        } else {
            fileObj[key] = fileObj[key].value
        }
    }

    return fileObj;
}

function getFinalCode() {
    console.log('xupdating......')
    var cssContent = fs.readFileSync('./dist/css/main.css', 'utf-8')
    var jsContent = fs.readFileSync('./dist/js/code.js', 'utf-8')
    var langContent = fs.readFileSync('./dist/cms/lang', 'utf-8')
    var htmlContent = fs.readFileSync('./dist/views/main.html', 'utf-8')
    console.log('updating......')
    var code = {
        js: jsContent,
        css: cssContent,
        lang: langContent,
        html: "${freeTextJSLiabraries} ${freetextcss} ${freeTextjs}" + htmlContent
    }

    var htmlInject = "${freeTextJSLiabraries} ${freetextcss} ${freeTextjs}"
    var result = `
    HTML
    ${htmlInject} ${htmlContent}


    JS
    ${jsContent}


    CSS
    ${cssContent}

    I18N
    ${langContent}
    `

    fs.writeFileSync('./dist/final', result)
}

/**
 * Compile pug files into HTML
 */
gulp.task('html', function() {
    return gulp
        .src(config.htmlPath)
        .pipe(plumber())
        .pipe(
            pug({
                data: {...updateTranslation(), ...getReferenceObject()}
            })
        )
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/'));
});


gulp.task('imgs', function() {
    return gulp
        .src(config.imgsPath)
        .pipe(gulp.dest('./dist/imgs'));
});


gulp.task('writeLangs', function(cb){
    var lang = getI18nFile(config.i18nPath)
    var langContent = Object.keys(lang).reduce((accum, key) => {
        return accum + key + '=' + lang[key] + ';'
    }, '')
    return file('lang', langContent,  { src: true })
             .pipe(gulp.dest('dist/cms'))
});

/**
 * Important!!
 * Separate task for the reaction to `.pug` files
 */
gulp.task('pug-watch', ['html'], function() {
    getFinalCode()
    return browserSync.reload();
});


function getI18nFile(filePath) {
    return JSON.parse(fs.readFileSync(filePath, 'utf8'))
}

var webpack = require('webpack-stream');

gulp.task('js', function(){
    return gulp.src(config.jsPath)
      .pipe(plumber())
      .pipe(webpack({
        mode: isProd ? "production": "development",
        output: {
            filename: 'code.js',
        }
      }))
      .on('error', function(e) {
        this.emit('end'); // Recover from errors
        })
      .pipe(plumber.stop())
      .pipe(gulp.dest('./dist/js'))
      .pipe(browserSync.stream());
  });


/**
 * Sass task for live injecting into all browsers
 */
gulp.task('sass', function() {
    return gulp
        .src(config.sassPath)
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('result', ['sass'], getFinalCode)

/**
 * Serve and watch the scss/pug files for changes
 */
gulp.task('default', ['sass', 'imgs', 'html', 'js', 'writeLangs', 'result'], function() {
    browserSync({ server: './dist', open: false });

    gulp.watch(config.sassPath, ['sass', 'result']);
    gulp.watch(config.htmlPath, ['pug-watch']);
    gulp.watch(config.i18nPath, ['writeLangs', 'pug-watch']);
    gulp.watch(config.cmsRefPath, ['pug-watch']);
    gulp.watch(config.jsPath, ['js', 'pug-watch']);
});
